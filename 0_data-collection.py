# %% [markdown]
# # Collect AESO Metered Volume Data

# %%
import requests
import pandas as pd
from datetime import datetime, timedelta
from dotenv import load_dotenv
import os

# %% [markdown]
# ## Define parameters

# Load environment variables from .env file
load_dotenv()

# Get the API token from environment variables
TOKEN = os.getenv('API_TOKEN')
if not TOKEN:
    raise ValueError('No API token provided. Please set the API_TOKEN environment variable.')

# API endpoint for metered volumes
API_ENDPOINT = 'https://api.aeso.ca/report/v1/meteredvolume/details'

# %%
# Power plant ID to fuel type mapping
asset_to_type = pd.read_csv('data/AESO_AssetMap_Full.csv', index_col=0)
asset_to_type['Energy Type'] = asset_to_type['Energy Type'].str.title()
asset_to_type = asset_to_type['Energy Type'].to_dict()

# %% [markdown]
# ## Define functions

# %%
# Function to fetch data for a given start and end date
def fetch_data(start_date, end_date):
    params = {
        "startDate": start_date.strftime("%Y-%m-%d"),
        "endDate": end_date.strftime("%Y-%m-%d"),
    }
    headers = {
        "accept": "application/json",
        "X-API-Key": TOKEN
    }
    
    response = requests.get(API_ENDPOINT, params=params, headers=headers)
    
    if response.status_code == 400:
        error_data = response.json()
        latest_allowed_date_str = error_data.get("message").split("end date of ")[1]
        latest_allowed_date = datetime.strptime(latest_allowed_date_str, "%Y-%m-%d")
        return fetch_data(start_date, latest_allowed_date)
    
    response.raise_for_status()  # Raise an exception for HTTP errors
    return response.json()['return']

# %%
def process_data(json_data):
    '''
    AESO returns data in a list of dictionaries, where each list item is
    one pool participant (i.e., company). The dictionary contains a nested
    list of assets (by ID) with metered volume for each hour.

    This function takes in the returned data and flattens the list to be
    useable by Pandas.
    '''
    flattened_data = []
    
    for participant in json_data:
        pool_participant_ID = participant['pool_participant_ID']
        
        for asset in participant['asset_list']:
            asset_ID = asset['asset_ID']
            asset_class = asset['asset_class']
            
            for metered_volume in asset['metered_volume_list']:
                begin_date_utc = metered_volume['begin_date_utc']
                metered_volume_value = metered_volume['metered_volume']
                
                flattened_data.append({
                    'pool_participant_ID': pool_participant_ID,
                    'asset_ID': asset_ID,
                    'asset_class': asset_class,
                    'begin_date_utc': begin_date_utc,
                    'metered_volume': metered_volume_value
                })
    
    return flattened_data
# %%
# # Debug
# params = {
#     'startDate': datetime(2024,6,16).strftime('%Y-%m-%d'),
#     'endDate': datetime(2024,6,17).strftime('%Y-%m-%d'),
#     # 'asset_ID': BLANK, # Facility ID
#     # 'pool_participant_ID': BLANK # Company ID
# }
# headers = {
#         'X-API-Key': f"{TOKEN}",
#         'accept': 'application/json'
#     }
# response = requests.get(API_ENDPOINT, params=params, headers=headers)

#  # Debugging: Print the response status code and content
# print(f"Response Status Code: {response.status_code}")
# print(f"Response Content: {response.text}")

# %% [markdown]
# ## Execute

# %%
# Collect data from a specified start to today that acknowledges the limits of the API (max 16 day increment for data)
start_date = datetime(2024, 6, 1)
end_date = datetime.today()

data = []

# This loop needs to iterate from the beginning to the end
current_start_date = start_date

while current_start_date < end_date:
    current_end_date = min(current_start_date + timedelta(days=15), end_date)
    print(f"Fetching data from {current_start_date.strftime('%Y-%m-%d')} to {current_end_date.strftime('%Y-%m-%d')}")
    batch_data = fetch_data(current_start_date, current_end_date)
    data.extend(batch_data)
    current_start_date = current_end_date + timedelta(days=1)

# %%
# Flatten the data
flattened_data = process_data(data)

# %%
# Convert data to a DataFrame
df = pd.DataFrame(flattened_data)
# df = df.query("asset_class=='IPP'") # Only keep generators
df['metered_volume'] = pd.to_numeric(df['metered_volume'])

# Map power plant IDs to energy types
df['energy_type'] = df['asset_ID'].map(asset_to_type)

# %%
# Convert the 'date' column to datetime
df['date'] = pd.to_datetime(df['begin_date_utc'])

# %%
# Save the data
df.to_excel('data/Compiled.xlsx')
# %%

# %% [markdown]

# # Estimated Costs of Constraint Report

# Congestion on transmission lines leads to curtailment of (or constrained generation from) wind and solar resources.
# This script compiles and analyzes the AESO's historical data The online version can be found at http://ets.aeso.ca/ets_web/ip/Market/Reports/EstimatedConstraintCostReportServlet?contentType=html and dates can be selected using a `?` to set data request parameters.

# For example:
# `http://ets.aeso.ca/ets_web/ip/Market/Reports/EstimatedConstraintCostReportServlet?beginDate=09012023&endDate=09012023&contentType=csv` downloads the constraint cost from September 1, 2023.

# Note: these are **estimated** figures. The [Annual Market Stastistics report](https://www.aeso.ca/market/market-and-system-reporting/annual-market-statistic-reports/)

# %%
import requests
import pandas as pd
import altair as alt
import datetime as date

# %%
# ## Define parameters 
# Pick a date
beginDate= pd.to_datetime('January 1, 2023').strftime('%m%d%Y')
endDate= pd.to_datetime('December 31, 2023').strftime('%m%d%Y')

baseURL = 'http://ets.aeso.ca/ets_web/ip/Market/Reports/EstimatedConstraintCostReportServlet'
queryURL = f"{baseURL}?beginDate={beginDate}&endDate={endDate}&contentType=csv"
# %%
df = pd.read_csv(queryURL, skiprows=4)
# %%
# Adjust the AESO hour-ending format to fit Pandas's
def adjustDatetime(inputDate):
    '''
    Assuming input is a string.
    
    Returns a string.
    '''
    dateStr, hourStr = inputDate.split(' ') # Split at the space
    hour = int(hourStr) - 1

    return f'{dateStr} {hour}'

# %%
df['Date (HE)'] = pd.to_datetime(
    df['Date (HE)'].apply(adjustDatetime),
    format = '%m/%d/%Y %H'
)

# %%
# Format the numbers as floats instead of strings
df['Estimated Cost ($)'] = df['Estimated Cost ($)'].str.replace(',','').astype(float)
df['Estimated TCR Charge ($/MWh)'] = df['Estimated TCR Charge ($/MWh)'].str.replace(',','').str.replace('∞','999.99').astype(float)

# %%
# ## Visualize

# %%
alt.Chart(df).mark_area().encode(
    x=alt.X('Date (HE):T'),
    y=alt.Y('Estimated Cost ($):Q')
)
# %%
# What hours of day do constraints happen at?
# How do constraints look like grouped by month?
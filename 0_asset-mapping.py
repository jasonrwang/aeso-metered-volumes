# %% [markdown]
# # AESO Asset Mapping
# Needs to get both current and historic.

# %%
import pandas as pd
from bs4 import BeautifulSoup
import requests

# %%
response = requests.get('http://ets.aeso.ca/ets_web/ip/Market/Reports/CSDReportServlet')

# %%
# Parse the HTML content using BeautifulSoup
soup = BeautifulSoup(response.content, 'html.parser')

# Separate the tables into the format that we want
# Find all wide tables (i.e., ignore the small ones)
tables = soup.find_all('table', {'width':'100%'})

# %%
def is_lowest_level_table(table):
    nested_tables = table.find_all('table')  # Find all nested tables within this table
    return len(nested_tables) == 0  # Return True if no nested tables are found

# Filter out the lowest level tables
tables = [table for table in tables if is_lowest_level_table(table)]

# Find the main table. It's the 5th one (index 4)
# Would be better to have a robust way to find it though.
tables = tables[4:]

# %%
# Initialize a list to store rows of the table
table_data = []

# Iterate over each energy type
for table in tables:
    energy_type = table.find_all('th')[0].string # There should only be one result per fuel type
    energy_category = energy_type # Default category to the energy type
    
    # Extract table rows
    rows = table.find_all('tr')
    
    for row in rows:
        # Replace the category if available
        if row.get('bgcolor') in ['#CEE3F6']:
            # print(row.string)
            energy_category = row.string
        
        if row.get('bgcolor') == '#336699':
            continue

        # Find now find all the assets, their names, and their IDs
        # Skip header rows and other non-data rows
        cols = row.find_all('td')
        if len(cols)>2:
            # Extract data from each column
            asset_name = cols[0].get_text(strip=True)
            maximum_capacity = cols[1].get_text(strip=True)

            # Append the row data as a tuple
            table_data.append((energy_type, energy_category, asset_name, maximum_capacity))
table_data

# %%
# Convert the list to a DataFrame
df = pd.DataFrame(table_data)

# # Drop empty rows and columns
# df.replace('', pd.NA, inplace=True)
# df.dropna(how='all', axis=0, inplace=True)
# df.dropna(how='all', axis=1, inplace=True)

# Set the first row as header
df.columns = ['Energy Type','Energy Category','Asset Full Name','Maximum Capacity']
# Remove some glitches because of inconsistencies on AESO site
df = df.query("`Asset Full Name`!='ASSET'")
# Drop duplicates
df = df.drop_duplicates()

# %%
# Extract the Asset ID and reorder the columns
pattern = r'\((.*?)\)'
df['Asset ID'] =  df['Asset Full Name'].str.extract(pattern)
df['Asset Name'] = df['Asset Full Name'].str.split('(').str[0]

df = df.reindex(columns=['Asset ID','Asset Full Name','Asset Name','Energy Type','Energy Category','Maximum Capacity'])

# %%
# df['Energy Type'] = df['Energy Type'].str.title()

# %%
df.set_index('Asset ID').to_csv('./data/AESO_AssetMap_Full.csv')
# %%

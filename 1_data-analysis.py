# %% [markdown]
# # AESO Metered Data Analysis

# %%
import altair as alt
import pandas as pd

# %% [markdown]
# ## Load the data
df = pd.read_excel('data/Compiled.xlsx')

# %% [markdown]
# ## Data Analysis

# Group by hour (default date) and fuel type, and sum the generation
df_grouped = df.groupby([df['begin_date_utc'], 'energy_type']).agg({'metered_volume': 'sum'}).reset_index()

# Can group by day, month, etc. too

# %%
# %% [markdown]
# ## Plot the data

# %%
# Create the Altair chart
chart = alt.Chart(df_grouped).mark_area().encode(
    x=alt.X('begin_date_utc:T',title='Date'),
    y=alt.Y('metered_volume:Q',title='Volume (MW)'),
    color=alt.Color('energy_type:N',title='Fuel Type')
).properties(
    title='Daily Electricity Generation by Fuel Type',
    width=800,
    height=400
)
chart

# %%
# Save the chart as an HTML file
# chart.save('electricity_generation.html')
